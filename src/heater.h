#ifndef HEATER_H
#define HEATER_H

#include <Arduino.h>
#include <esp8266wifi.h>
#include <esp8266httpclient.h>
#include <ArduinoJson.h>
#include <BlynkSimpleEsp8266.h>
#include <ESPAsyncUDP.h>
#include "config.h"
#include "gpioswitch.h"
//#include "oled.h"

#define HEATER_SYSTEM_MODE_KEY "systemMode"
#define HEATER_TEMP_TARGET_KEY "tempTarget"


#define HEATER_TEMP_TARGET_MIN 6
#define HEATER_TEMP_TARGET_MAX 26
#define HEATER_TEMP_TARGET_DEFAULT 20

#define HEATER_TEMP_CURRENT_MIN -20
#define HEATER_TEMP_CURRENT_MAX 50


#define HEATER_OPTOCOUPLER_GPIO 15 // u need a resistor ~ 1 kohm => 2.22 mA. My heater transmits 0.5mA over optocoupler's collector
#define HEATER_SWITCH_MANUAL_GPIO 13 // Fixed button to override
#define HEATER_CONFIG_FILE "heater.json"

#define HEATER_SWITCH_MANUAL_CHECK_INTERVAL 100


#define HEATER_ON_MIN_INTERVAL 30*1000
#define HEATER_AUTO_OFF_IF_NO_READINGS_INTERVAL 60*1000
#define HEATER_TEMP_GAP 0.3

#define HEATER_MULTICAST_PORT 3333

#define HEATER_MULTICAST_TEMPERATURE_KEY "temperature"

#define HEATER_BLYNK_MODE_AUTO_BUTTON_VPIN 0
#define HEATER_BLYNK_MODE_AUTO_LED_VPIN 7
#define HEATER_BLYNK_MANUAL_OVERRIDE_VPIN 2
#define HEATER_BLYNK_HEATER_STATE_VPIN 3
#define HEATER_BLYNK_TEMP_CURRENT_VPIN 4
#define HEATER_BLYNK_TARGET_TEMP_SET_VPIN 5
#define HEATER_BLYNK_TARGET_TEMP_VPIN 6
#define HEATER_BLYNK_TERMINAL_VPIN 1

#define HEATER_BLYNK_MODE_AUTO_BUTTON_READ_DELAY 3000

#define HEATER_LOG_FROM_ASYNC_V_SIZE 20

struct HEATER_LOG_FROM_ASYNC_STRUCT {
    String msg;
    uint level;
};


class Heater
{
 
public:
    static bool modeAuto;
    static bool manualOverride;
    static bool heaterState;
    static ulong manualOverrideLastRead;
    static float tempCurrent;
    static ulong tempCurrentLastRead;
    static bool tempCurrentActual;
    static int tempTarget;
    static AsyncUDP udp;
    static WidgetTerminal terminal;
    static ulong modeAutoButtonLastRead;
    static std::vector<HEATER_LOG_FROM_ASYNC_STRUCT> logFromAsyncV;
    

    static void setup()
    {
        pinMode(HEATER_OPTOCOUPLER_GPIO, OUTPUT);
//        pinMode(HEATER_BUTTON_THERMOSTAT_GPIO, INPUT_PULLUP);
        pinMode(HEATER_SWITCH_MANUAL_GPIO, INPUT_PULLUP);
        setHeaterState(false);
        configLoad();

        setManualOverride(digitalRead(HEATER_SWITCH_MANUAL_GPIO));
        
        Blynk.virtualWrite(HEATER_BLYNK_MANUAL_OVERRIDE_VPIN, manualOverride);
        Blynk.run();

        if (udp.listenMulticast(IPAddress(239,1,1,2), HEATER_MULTICAST_PORT))
        {
            logFromAsync("Listening on port:" + String(HEATER_MULTICAST_PORT),LOG_INFO);
            udp.onPacket([](AsyncUDPPacket packet) {
                String packetData = "";
                for (size_t i = 0; i < packet.length(); i++)
                {
                    packetData += (char)*(packet.data() + i);
                }
                String m = "UDP Packet Type: ";
                m += packet.isBroadcast() ? "Broadcast" : packet.isMulticast() ? "Multicast" : "Unicast";
                m += ", From: " + packet.remoteIP().toString() + ":" + String(packet.remotePort()) + ", To: " + packet.localIP().toString() + ":";
                m += String(packet.localPort()) + ", Length: " + String(packet.length()) + ", Data: " + packetData;
                logFromAsync(m, LOG_DEBUG);

                StaticJsonDocument<512> doc;
                DeserializationError error = deserializeJson(doc, packetData);
                if (error)
                {
                    logFromAsync("Failed to parse JSON.", LOG_ERR);
                    return;
                }
                if (doc[HEATER_MULTICAST_TEMPERATURE_KEY].isNull())
                {
                    logFromAsync("Key " + String(HEATER_MULTICAST_TEMPERATURE_KEY) + " missing", LOG_ERR);
                    return;
                }

                if (validateTempCurrent(doc[HEATER_MULTICAST_TEMPERATURE_KEY].as<float>())) {
                    tempCurrent = doc[HEATER_MULTICAST_TEMPERATURE_KEY].as<float>();
                    tempCurrentLastRead = millis();
                    tempCurrentActual = true;
                    announce();
                }
            });
        }

    }

    static bool validateTempTarget(int v)
    {
        if (v >= HEATER_TEMP_TARGET_MIN && v <= HEATER_TEMP_TARGET_MAX)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    static bool validateTempCurrent(int v)
    {
        if (v >= HEATER_TEMP_CURRENT_MIN && v <= HEATER_TEMP_CURRENT_MAX)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    static void configLoad()
    {
        log("Loading config...", LOG_INFO);

        File file = SPIFFS.open(HEATER_CONFIG_FILE, "r");
        if (!file)
        {
            log("Failed to read file:" + String(HEATER_CONFIG_FILE), LOG_ERR);
            return;
        }
        String c = "";
        while (file.available())
            c += char(file.read());
        log("Config file contents: \n " + c, LOG_INFO);
        file.close();

        StaticJsonDocument<512> doc;
        DeserializationError error = deserializeJson(doc, c);
        if (error)
        {
            log("Failed to parse JSON.", LOG_ERR);
            return;
        }
        if (doc[HEATER_TEMP_TARGET_KEY].isNull()) {
            log("Key: " + String(HEATER_TEMP_TARGET_KEY) + " is missing, unable to parse config!", LOG_ERR);
        } else
        {
            if (validateTempTarget(doc[HEATER_TEMP_TARGET_KEY].as<int>()))
            {
                log("Valid tempCurrent, will use it", LOG_INFO);
                tempTarget = doc[HEATER_TEMP_TARGET_KEY].as<int>();
            }
            else
            {
                log("Invalid tempCurrent: " + doc[HEATER_TEMP_TARGET_KEY].as<String>(), LOG_ERR);
            }
        }

        if (doc[HEATER_SYSTEM_MODE_KEY].isNull()) {
            log("Key: " + String(HEATER_SYSTEM_MODE_KEY) + " is missing, unable to parse config!", LOG_ERR);
        } else {
            log("Valid mode, will use it:" + doc[HEATER_SYSTEM_MODE_KEY].as<String>(), LOG_INFO);
            modeAuto = doc[HEATER_SYSTEM_MODE_KEY].as<bool>();
        }
    }

    static void configSave()
    {
        File file = SPIFFS.open(HEATER_CONFIG_FILE, "w");
        if (!file)
        {
            log("Failed to create file:" + String(HEATER_CONFIG_FILE), LOG_ERR);
            return;
        }

        StaticJsonDocument<200> doc;
        doc[HEATER_SYSTEM_MODE_KEY] = modeAuto;
        if (tempTarget != -100)
            doc[HEATER_TEMP_TARGET_KEY] = tempTarget;
        String c;
        serializeJson(doc, file);
        serializeJson(doc, c);
        log("State saved as: \n" + c, LOG_DEBUG);
        file.close();
    }

    static void setTargetTemp(int t) {
        if (validateTempTarget(t)) {
            tempTarget = t;
            configSave();
            Blynk.virtualWrite(HEATER_BLYNK_TARGET_TEMP_VPIN, tempTarget);
            Blynk.setProperty(HEATER_BLYNK_TARGET_TEMP_VPIN, "label", "TEMP TARGET " + Timetools::formattedDateTime());
            Blynk.run();
        }
    }

    static void setmodeAuto(bool m) {
        modeAuto = m;
        if (!m && heaterState) {
            setHeaterState(false);
        }
        configSave();
    }
    static void setHeaterState(bool s) {
        digitalWrite(HEATER_OPTOCOUPLER_GPIO, s);
        s ? Blynk.virtualWrite(HEATER_BLYNK_HEATER_STATE_VPIN, 255) : Blynk.virtualWrite(HEATER_BLYNK_HEATER_STATE_VPIN, 0);
        heaterState = s;
        s ? terminal.println(Timetools::formattedDateTime() + " HEATER: ON") : terminal.println(Timetools::formattedDateTime() + " HEATER: OFF");
        terminal.flush();
        Blynk.run();
    }
    static void setManualOverride(bool s) {
        s ? Blynk.virtualWrite(HEATER_BLYNK_MANUAL_OVERRIDE_VPIN, 255) : Blynk.virtualWrite(HEATER_BLYNK_MANUAL_OVERRIDE_VPIN, 0);
        manualOverride = s;
        s ? terminal.println(Timetools::formattedDateTime() + " MANUAL OVERRIDE: ON") : terminal.println(Timetools::formattedDateTime() + " MANUAL OVERRIDE: OFF");
        terminal.flush();
        log("manualOverride: " + String(manualOverride), LOG_INFO);
        Blynk.run();
    }


    static void handle()
    {
        //
        logFromAsyncHandle();
        
        
        //manual mode switch
        if (millis() - manualOverrideLastRead> HEATER_SWITCH_MANUAL_CHECK_INTERVAL)
        {
            manualOverrideLastRead = millis();
            if (digitalRead(HEATER_SWITCH_MANUAL_GPIO) == 0 && manualOverride == false)
            {
                setManualOverride(true);
            }
            if (digitalRead(HEATER_SWITCH_MANUAL_GPIO) == 1 && manualOverride == true)
            {
                setManualOverride(false);
            }
            
        }
        if (manualOverride) return;
        
        
        if (tempCurrentActual && millis() - tempCurrentLastRead > HEATER_AUTO_OFF_IF_NO_READINGS_INTERVAL) {
            String m = "It's been over " + String(HEATER_AUTO_OFF_IF_NO_READINGS_INTERVAL) + " ms since I got any tempCurrent readings";
            log(m,LOG_ERR);
            tempCurrentActual = false;
            setHeaterState(false);
            Blynk.virtualWrite(HEATER_BLYNK_TEMP_CURRENT_VPIN, "-");
            Blynk.setProperty(HEATER_BLYNK_TEMP_CURRENT_VPIN, "label", "TEMP CURRENT " + Timetools::formattedDateTime());
            terminal.println(Timetools::formattedDateTime() + " " + m);
            terminal.flush();
            Blynk.run();
        }

        if (modeAuto && tempCurrentActual && !heaterState && tempTarget - tempCurrent > HEATER_TEMP_GAP) {
            log("setHeaterState(true)",LOG_INFO);
            setHeaterState(true);
        }

        if (modeAuto && tempCurrentActual && heaterState && tempCurrent >= tempTarget) {
            log("setHeaterState(false)",LOG_INFO);
            setHeaterState(false);
        }
    }

    static void log(String msg, uint level = LOG_INFO)
    {
        GelfUdpLogger::log(msg, level);
        if (level <= LOG_INFO ) Logger::syslog(msg);
    }

    static void logFromAsync(String msg, uint level = LOG_INFO)
    {
        while (logFromAsyncV.size() > HEATER_LOG_FROM_ASYNC_V_SIZE) logFromAsyncV.erase(logFromAsyncV.begin());
        logFromAsyncV.push_back({msg,level});
    }

    static void logFromAsyncHandle() {
        for (auto i: logFromAsyncV) {
            log(i.msg,i.level);
        }
        logFromAsyncV.clear();
    }


    static void announce() {
        heaterState ? Blynk.virtualWrite(HEATER_BLYNK_HEATER_STATE_VPIN, 255) : Blynk.virtualWrite(HEATER_BLYNK_HEATER_STATE_VPIN, 0);
        manualOverride ? Blynk.virtualWrite(HEATER_BLYNK_MANUAL_OVERRIDE_VPIN, 255) : Blynk.virtualWrite(HEATER_BLYNK_MANUAL_OVERRIDE_VPIN, 0);
        Blynk.virtualWrite(HEATER_BLYNK_TARGET_TEMP_VPIN, tempTarget);
        Blynk.setProperty(HEATER_BLYNK_TARGET_TEMP_VPIN, "label", "TEMP TARGET [" + Timetools::formattedDateTime() +"]");
        tempCurrentActual ? Blynk.virtualWrite(HEATER_BLYNK_TEMP_CURRENT_VPIN, tempCurrent) : Blynk.virtualWrite(HEATER_BLYNK_TEMP_CURRENT_VPIN, "-");
        Blynk.setProperty(HEATER_BLYNK_TEMP_CURRENT_VPIN, "label", "TEMP CURRENT [" + Timetools::formattedDateTime() +"]");
        modeAuto ? Blynk.virtualWrite(HEATER_BLYNK_MODE_AUTO_LED_VPIN, 255) : Blynk.virtualWrite(HEATER_BLYNK_MODE_AUTO_LED_VPIN, 0);
        Blynk.run();
    }

    static String getWebInfo() {
        String t = "";
        t += "<strong>--- Heater data: </strong>";
        t += "modeAuto: " + String(modeAuto) + ", manualOverride: " + String(manualOverride) + \
            ", tempCurrent: " + String(tempCurrent) + ", tempCurrentActual: " + String(tempCurrentActual) + 
            ", tempCurrentLastRead: " + Timetools::formattedDateTimeFromMillis(tempCurrentLastRead) + ", tempTarget: " + String(tempTarget) +
            ", heaterState: " + String(heaterState);
        return t;
    }
};

bool Heater::modeAuto = false;
bool Heater::manualOverride = false;
ulong Heater::manualOverrideLastRead = 0;
float Heater::tempCurrent = -100;
ulong Heater::tempCurrentLastRead = 0;
bool Heater::tempCurrentActual = false;
int Heater::tempTarget = -100;
bool Heater::heaterState = false;
AsyncUDP Heater::udp;
WidgetTerminal Heater::terminal(HEATER_BLYNK_TERMINAL_VPIN);
ulong Heater::modeAutoButtonLastRead = 0;
std::vector<HEATER_LOG_FROM_ASYNC_STRUCT> Heater::logFromAsyncV;

BLYNK_WRITE(HEATER_BLYNK_TARGET_TEMP_SET_VPIN)
{
  int temp = param.asInt();
  Heater::log("BLYNK requested tempTarget : " + String(temp), LOG_INFO);
  Heater::setTargetTemp(temp);
}

BLYNK_WRITE(HEATER_BLYNK_MODE_AUTO_BUTTON_VPIN)
{
    if (millis() - Heater::modeAutoButtonLastRead > HEATER_BLYNK_MODE_AUTO_BUTTON_READ_DELAY) {
        Heater::modeAutoButtonLastRead = millis();
        int temp = param.asInt();
        if (temp == 1 ) {
        Heater::log("BLYNK requested to change modeAuto: " + String(!Heater::modeAuto), LOG_INFO);
        Heater::setmodeAuto(!Heater::modeAuto);
        Heater::announce();
    } else {
        Heater::log("Button MODE_AUTO clicked to fast, wait for " + String(HEATER_BLYNK_MODE_AUTO_BUTTON_READ_DELAY/100) + " sec", LOG_INFO);
    }
  }
  
}

#endif