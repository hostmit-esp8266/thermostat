#ifndef BLYNKMY_H
#define BLYNKMY_H

#include <Arduino.h>
#include <BlynkSimpleEsp8266.h>
#include "logger.h"
#include "config.h"
#include "sysinfo.h"
#include "heater.h"
#include "credentials.h"

class BlynkMy
{
public:
  static long buttonOnOfMillis;

  static void setup()
  {

    Blynk.config(BLYNK_TOKEN, BLYNK_HOST, BLYNK_PORT);
  }

  static void handle()
  {

    Blynk.run();
  }

  static void log(String msg, uint level = 6)
  {
    Logger::syslog(msg);
  }
};

long BlynkMy::buttonOnOfMillis = 0;

BLYNK_CONNECTED()
{
  BlynkMy::log("Blynk connected to host: " + String(BLYNK_HOST) + ", port: " + String(BLYNK_HOST) + ", token: " + String(BLYNK_TOKEN), 6);
  Heater::announce();
}

BLYNK_WRITE(InternalPinOTA)
{
  Logger::syslog("InternalOta update requested");
  auto overTheAirURL = param.asString();
  Logger::syslog(overTheAirURL);
};

#endif
